#!/bin/bash
if [[ $# < 1 ]]; then
  cat <<HELP
Built ROLE on the development server

Usage: $0 <ROLE> [ansible-playbook options]

Examples:
  $0 eddbase
  $0 edd_master_controller -vv
HELP
  exit
fi

ROLE=$1
shift

echo "Building role \"$ROLE\" ..."

export ANSIBLE_ROLES_PATH="$(pwd)/roles"
export ANSIBLE_RETRY_FILES_ENABLED="False"


BUILDHOST="build_server[0]"


ansible-playbook "$@" -e base_path=`pwd` --tags=build,deploy /dev/stdin <<END
---
- hosts: localhost
  gather_facts: no
  vars:
    ansible_hostname: $BUILDHOST 
  tasks:
  - name: Print path variable
    debug:
      var: base_path
    tags: build

  roles:
    - $ROLE
END
