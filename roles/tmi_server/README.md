# Telescope Meta Information (TMI) Server

The TMI server provides meta information of the telescope and observation (e.g. source-name, scan number).

The TMIServer-class in `mpikat/core/telescope_meta_information_server.py` is a base class for telescope specific meta data interfaces. For instance, concret implementation of the TMIServer are the SKAMPIStatusServer or the EffelsbergStatusServer.

The TMI server pushes the latest information to the Redis database TELESCOPE_META (:4), so that EDD pipelines (products) are able to access the meta information. On a `measurement-prepare` the Master Controller ensures the TMI server has retrieved and updated the latest information for the actual scan. On the one hand this is a desired behavior as several pipelines require the latest meta information before a measurement can start, on the other hand the Master Controller requires a connection to a running TMI server in order to prepare the next measurement. If this is not given, the Master Controller goes into an error state.

In case, you have no concrete implementation of the TMI server for your telescope, you can use the `tmi_server`-role instead. The `tmi_server` is a no-op server which only response to the Master Controller without retrieving or updating meta information.

Note: Some products like the `pulsar_pipeline` rely on the meta information and may not work.