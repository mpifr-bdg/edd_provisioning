FROM {{docker_registry}}:{{docker_registry_port}}/nvidia/cuda:12.6.1-devel-ubuntu24.04

MAINTAINER Tobias Winchen "twinchen@mpifr-bonn.mpg.de"

# Suppress debconf warnings
ENV DEBIAN_FRONTEND noninteractive

USER root

RUN apt-get -y update && apt-get install -y ca-certificates
# Inject aptly source into
COPY ./*.sources /etc/apt/sources.list.d/
COPY mpifr-bdg.asc /etc/apt/trusted.gpg.d/mpifr-bdg.asc
RUN chmod a+r /etc/apt/trusted.gpg.d/* && apt-get update -y


# Local override for cuda libraries so that dpkg / debuild can find them.
RUN echo 'libcudart 12 cuda-cudart-12-6' >> /etc/dpkg/shlibs.override

# Install libraries from ubuntu for build, and possibly dependencies
RUN    apt-get -y check && \
    apt-get -y update && \
    apt-get install -y apt-utils apt-transport-https software-properties-common locales && \
    apt-get -y update --fix-missing && \
    apt-get -y upgrade && \
    echo en_US.UTF-8 UTF-8 > /etc/locale.gen && \
    echo LANG=en_US.UTF-8 > /etc/default/locale && \
    locale-gen  && \
    apt -y autoremove && \
    # Install dependencies \
    apt-get --no-install-recommends -y install \
      build-essential \
      autoconf \
      iputils-ping \
      autotools-dev \
      automake \
      autogen \
      libtool \
      csh \
      gcc \
      gfortran \
      wget \
      cmake \
      git \
      git-lfs \
      less \
      cvs \
      expect \
      libcfitsio-dev \
      libltdl-dev \
      gsl-bin \
      libgsl-dev \
      libgslcblas0 \ 
      hwloc \
      libhwloc-dev \
      pkg-config \
      net-tools \
      iproute2 \
      htop \
      kmod \
      vim \
      ethtool \
      lsof \
      pciutils \
      numactl \
      libboost-all-dev \
      librdmacm-dev \
      ibverbs-utils \
      libibverbs-dev \
      python-is-python3 \
      gnupg \ 
      devscripts \
      debhelper \
      debhelper-compat \
      git-buildpackage && \
      apt-get clean

WORKDIR /root
