FROM {{docker_registry}}:{{docker_registry_port}}/eddbase:{{version_tag}}
MAINTAINER Jason Wu "jwu@mpifr-bonn.mpg.de"

# Suppress debconf warnings
ENV DEBIAN_FRONTEND noninteractive

USER root

# Software sources go to /src

# Additional dependencies
RUN   cd /src && \
    apt-get update && \
    apt-get --no-install-recommends -y install \
    libxml2 \
    libxml2-dev \
    pgplot5 \
    autoconf \
    autotools-dev  \
    automake  \
    autogen  \
    libtool  \
    pkg-config \
    cmake  \
    csh  \
    gcc  \
    gfortran  \
    wget  \
    git  \
    expect \
    cvs  \
    libcfitsio-dev  \
    hwloc  \
    libfftw3-3  \
    libfftw3-bin  \
    libfftw3-dev  \
    libfftw3-single3  \
    libx11-dev \
    libpnglite-dev \
    libhdf5-dev  \
    libhdf5-serial-dev  \
    libxml2  \
    libxml2-dev  \
    libltdl-dev  \
    gsl-bin  \
    swig \
    libgsl-dev \
    python3-watchdog

# PGPLOT
ENV PGPLOT_DIR /usr/lib/pgplot5
ENV PGPLOT_FONT /usr/lib/pgplot5/grfont.dat
ENV PGPLOT_INCLUDES /usr/include
ENV PGPLOT_BACKGROUND white
ENV PGPLOT_FOREGROUND black
ENV PGPLOT_DEV /xs
ENV PSRHOME /src/
ENV OSTYPE linux
ENV CUDA_HOME /usr/local/cuda
ENV CUDA_ROOT /usr/local/cuda
ENV TEMPO2=/usr/share/tempo2
ENV TEMPO=/usr/share/tempo

WORKDIR $PSRHOME

RUN mv psrchive-2021-01-18 psrchive && \
    git clone git://git.code.sf.net/p/dspsr/code dspsr && \

RUN apt-get -y install epsic calceph dal psrcat psrxml tempo2 tempo psarchive


WORKDIR $PSRHOME
RUN echo "Predictor::default = tempo2" >> .psrchive.cfg && \
    echo "Predictor::policy = default" >> .psrchive.cfg

ENV DSPSR=$PSRHOME"/dspsr"
ENV PATH=$PATH:$PSRHOME"/dspsr/install/bin"
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PSRHOME"/dspsr/install/lib"
ENV C_INCLUDE_PATH=$C_INCLUDE_PATH:$PSRHOME"/dspsr/install/include"
WORKDIR $DSPSR
RUN git checkout {{ dspsr_version }} && \
    sed -i s/-lgsl/-lgsl\ -fopenmp/ /usr/local/bin/psrdada_ldflags && \
    ./bootstrap && \
    echo "dada fits sigproc" > backends.list && \
    ./configure --prefix=$DSPSR/install --with-cuda-lib-dir=/usr/local/cuda/lib64/ --with-cuda-include-dir=/usr/local/cuda/include/ --x-libraries=/usr/lib/x86_64-linux-gnu CPPFLAGS="-I"$DAL"/install/include -I/usr/include/hdf5/serial -I"$PSRXML"/install/include -I/usr/local/include/psrdada" LDFLAGS="-L"$DAL"/install/lib -L/usr/lib/x86_64-linux-gnu/hdf5/serial -L"$PSRXML"/install/lib" LIBS="-lpgplot -lcpgplot -lpsrxml -lxml2"  && \
    make -j $(nproc) && \
    make && \
    make install && \
    cd Benchmark && \ 
    /bin/bash filterbank_bench.csh && \
    cp filterbank_bench.out /src/psrchive/install/share/filterbank_bench_CUDA.dat

WORKDIR /src/


